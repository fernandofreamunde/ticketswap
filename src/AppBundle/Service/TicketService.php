<?php
/**
 * Created by PhpStorm.
 * User: fernando
 * Date: 21-6-17
 * Time: 14:30
 */

namespace AppBundle\Service;

use AppBundle\Entity\Listing;
use AppBundle\Entity\Ticket;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class TicketService
{
    private $user;
    private $em;
    private $TicketRepo;
    private $barcodeService;

    function __construct (ContainerInterface $container, BarcodeService $barcodeService)
    {
        $session = new Session();

        $this->barcodeService = $barcodeService;
        $this->em = $container->get('doctrine')->getManager();
        if ($session->get('user_id'))
        {
            $this->user = $this->em->getRepository('AppBundle:User')
                ->find($session->get('user_id'));
        }
        $this->TicketRepo = $this->em->getRepository('AppBundle:Ticket');
    }

    public function newTicket($listingID, $barcodeToSet)
    {
        $listing = $this->em->getRepository('AppBundle:Listing')
            ->find($listingID);

        $ticket = $this->getTicket();
        $ticket->setListing($listing);

        $this->em->persist($ticket);
        $this->em->flush();

        $this->barcodeService->createBarcode($ticket, $barcodeToSet);

        return $ticket;
    }

    public function buyTicket($id)
    {
        $ticket = $this->getTicket($id);

        $ticket->setBuyer($this->user);
        $ticket->setBoughtAt(new \DateTime());

        $this->em->persist($ticket);
        $this->em->flush();

        return $ticket;
    }

    private function getTicket($id = null)
    {
        if ($id != null)
        {
            return $this->TicketRepo->find($id);
        }

        return new Ticket();
    }

    public function getAllByListing(Listing $listing)
    {
        $tickets = $this->TicketRepo->findAllByListing($listing->getId());

        //init all user objects to have the user names
        foreach ($tickets as $ticket)
        {
            /**
             * @var $ticket Ticket
             */
            if ($ticket->getBuyer())
            {
                $ticket->getBuyer()->getName();
            }
        }

        return $tickets;
    }
}