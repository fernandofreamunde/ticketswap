<?php
/**
 * Created by PhpStorm.
 * User: fernando
 * Date: 21-6-17
 * Time: 14:04
 */

namespace AppBundle\Service;

use Psr\Container\ContainerInterface;

class UserService
{
    private $container;

    function __construct (ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function validateUser($id)
    {
        $em = $this->container->get('doctrine')->getManager();
        $user = $em->getRepository('AppBundle:User')
            ->find($id);

        if ($user === null)
        {
            return false;
        }

        return $user;
    }
}