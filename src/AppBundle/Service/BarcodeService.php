<?php
/**
 * Created by PhpStorm.
 * User: fernando
 * Date: 21-6-17
 * Time: 16:04
 */

namespace AppBundle\Service;
use AppBundle\Entity\Barcode;
use AppBundle\Entity\Ticket;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class BarcodeService
{
    private $user;
    private $em;

    function __construct (ContainerInterface $container)
    {
        $session = new Session();

        $this->em   = $container->get('doctrine')->getManager();
        if ($session->get('user_id'))
        {
            $this->user = $this->em->getRepository('AppBundle:User')
                ->find($session->get('user_id'));
        }
    }

    public function validateBarcode($newBarcode)
    {
        $barcode = $this->getBarcode($newBarcode);

        if ($barcode)
        {
            $buyer = $barcode->getTicket()->getBuyer();

            if (!$buyer || $buyer->getName() !== $this->user->getName() || !$barcode->getTicket()->getBuyer())
            {
                return false;
            }
        }

        return true;
    }

    public function createBarcode(Ticket $ticket, $newBarcode)
    {
        $barcode = $this->getBarcode();
        $barcode->setBarcode($newBarcode);
        $barcode->setTicket($ticket);

        $this->em->persist($barcode);
        $this->em->flush();

        return $barcode;
    }

    public function getBarcode($barcode = null)
    {
        if ($barcode != null)
        {
            return $this->em->getRepository('AppBundle:Barcode')
                ->findOneBy(['barcode'=>$barcode]);
        }

        return new Barcode();
    }
}