<?php
/**
 * Created by PhpStorm.
 * User: fernando
 * Date: 21-6-17
 * Time: 23:32
 */

namespace AppBundle\Service;

use AppBundle\Entity\Listing;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class ListingService
{
    private $user;
    private $em;
    private $ListingRepo;

    function __construct (ContainerInterface $container)
    {
        $session = new Session();

        $this->em                = $container->get('doctrine')->getManager();
        $this->ListingRepo = $this->em->getRepository('AppBundle:Listing');

        if ($session->get('user_id'))
        {
            $this->user = $this->em->getRepository('AppBundle:User')
                ->find($session->get('user_id'));
        }
    }

    public function getAllListings()
    {
        return $this->ListingRepo->findAll();
    }

    public function createListing($description, $price)
    {
        $listing = $this->getListing();

        $listing->setDescription($description);
        $listing->setPrice($price);
        $listing->setAuthor($this->user);

        $this->em->persist($listing);
        $this->em->flush();

        return $listing;
    }

    public function getListing($listing = null)
    {
        if ($listing != null)
        {
            return $this->ListingRepo->find($listing);
        }

        return new Listing();
    }
}