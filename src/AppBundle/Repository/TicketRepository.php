<?php
/**
 * Created by PhpStorm.
 * User: fernando
 * Date: 20-6-17
 * Time: 9:25
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class TicketRepository extends EntityRepository
{
    public function findAllByListing($listingId)
    {
        return $this->createQueryBuilder('ticket')
            ->andWhere('ticket.listing = :listingId')
            ->setParameter(':listingId', $listingId)
            ->getQuery()
            ->execute();
    }
}