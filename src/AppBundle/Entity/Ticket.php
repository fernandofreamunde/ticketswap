<?php
/**
 * Created by PhpStorm.
 * User: fernando
 * Date: 19-6-17
 * Time: 17:17
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TicketRepository")
 * @ORM\Table(name="ticket")
 */
class Ticket
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Listing")
     */
    private $listing;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $buyer;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $boughtAt;

    /**
     * @return mixed
     */
    public function getId ()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getListing ()
    {
        return $this->listing;
    }

    /**
     * @param mixed $listing
     */
    public function setListing (Listing $listing)
    {
        $this->listing = $listing;
    }

    /**
     * @return mixed
     */
    public function getBuyer ()
    {
        return $this->buyer;
    }

    /**
     * @param mixed $buyer
     */
    public function setBuyer (User $buyer)
    {
        $this->buyer = $buyer;
    }

    /**
     * @return mixed
     */
    public function getBoughtAt ()
    {
        return $this->boughtAt;
    }

    /**
     * @param mixed $boughtAt
     */
    public function setBoughtAt ($boughtAt)
    {
        $this->boughtAt = $boughtAt;
    }
}