<?php
/**
 * Created by PhpStorm.
 * User: fernando
 * Date: 19-6-17
 * Time: 15:07
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Listing;
use AppBundle\Entity\Ticket;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class ListingController extends Controller
{
    private $session;

    function __construct ()
    {
        $this->session = new Session();

        if (!$this->session->get('user'))
        {
            $this->session->set('user', 'guest');
        }
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $listingService = $this->get('app.service.listing');
        $listings = $listingService->getAllListings();

        return $this->render('listing/index.html.twig', [
            'user' => $this->session->get('user'),
            'listings' => $listings
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/listing/new", name="newListing")
     */
    public function newAction()
    {
        if ($this->session->get('user') === 'guest')
        {
            return $this->redirectToRoute('homepage');
        }

        return $this->render('listing/new.html.twig', [
            'user' => $this->session->get('user'),
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/listing/submit", name="submitListing")
     */
    public function listingValidationAction(Request $request)
    {
        $description = $request->request->get('description');
        $price = $request->request->get('price');

        $listingService = $this->get('app.service.listing');
        $listing = $listingService->createListing($description, $price);

        return $this->redirectToRoute('showListing', ['listingId' => $listing->getId()]);
    }

    /**
     * @Route("/listing/{listingId}", name="showListing")
     */
    public function showAction($listingId)
    {
        $listingService = $this->get('app.service.listing');
        $ticketService = $this->get('app.service.ticket');

        $listing = $listingService->getListing($listingId);

        if (!$listing)
        {
            return $this->redirectToRoute('homepage');
        }

        $tickets = $ticketService->getAllByListing($listing);

        //Symfony probably have a better way of dealing with this...
        $errorbag = [];
        if ($this->session->get('error'))
        {
            $errorbag = $this->session->get('error');
            $this->session->remove('error');
        }

        return $this->render('listing/show.html.twig', [
            'user'    => $this->session->get('user'),
            'listing' => $listing,
            'errorbag' => $errorbag,
            'tickets' => $tickets
        ]);
    }
}