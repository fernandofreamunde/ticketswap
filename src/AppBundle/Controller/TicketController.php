<?php
/**
 * Created by PhpStorm.
 * User: fernando
 * Date: 19-6-17
 * Time: 17:35
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class TicketController extends Controller
{
    private $session;

    function __construct ()
    {
        $this->session = new Session();
    }

    /**
     * @Route("/ticket/submit", name="submitTicket")
     */
    public function newAction(Request $request)
    {
        $listingID = $request->request->get('listing');
        $barcodeToSet = $request->request->get('barcode');

        $ticketService = $this->get('app.service.ticket');
        $barcodeService = $this->get('app.service.barcode');

        if (!$barcodeService->validateBarcode($barcodeToSet))
        {
            $this->session->set('error', 'Barcode Invalid');
            return $this->redirectToRoute('showListing',['listingId' => $listingID]);
        }

        $ticketService->newTicket($listingID, $barcodeToSet);

        return $this->redirectToRoute('showListing',['listingId' => $listingID]);
    }

    /**
     * @Route("/ticket/buy/{ticketId}", name="buyTicket")
     */
    public function buy($ticketId)
    {
        if ($this->session->get('user') == 'guest')
        {
            return $this->redirectToRoute('userLogin');
        }

        $ticketService = $this->get('app.service.ticket');
        $ticket = $ticketService->buyTicket($ticketId);

        return $this->redirectToRoute('showListing',['listingId' => $ticket->getListing()->getId()]);
    }
}