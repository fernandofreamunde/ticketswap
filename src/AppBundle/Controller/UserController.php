<?php
/**
 * Created by PhpStorm.
 * User: fernando
 * Date: 19-6-17
 * Time: 12:54
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class UserController extends Controller
{
    private $session;

    function __construct ()
    {
        $this->session = new Session();

        if (!$this->session->get('user'))
        {
            $this->session->set('user', 'guest');
        }
    }

    /**
     * @Route("/user/login", name="userLogin")
     */
    public function loginAction()
    {
        return $this->render('user/login.html.twig', ['user' => $this->session->get('user')]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/user/login/submit", name="submitLogin")
     */
    public function loginValidationAction(Request $request)
    {
        $userId = $request->request->get('user_id');

        $userService = $this->get('app.service.user');

        if (!$user = $userService->validateUser($userId))
        {
            return $this->redirectToRoute('userLogin');
        }

        $this->session->set('user', $user->getName());
        $this->session->set('user_id', $user->getId());

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/user/logout", name="logout")
     */
    public function logoutAction()
    {
        $this->session->clear();

        return $this->redirectToRoute('homepage');
    }
}