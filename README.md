## Mini-Ticketswap

This project was initiated as an assessment for a Web Developer position at [Ticketswap](https://www.ticketswap.com/).

The task in hand is to implement a super simple system where users can create a listings that can contain multiple tickets.

Providing barcodes to tickets can be done just by a simple input. Tickets can contain multiple barcodes.

Before you can create a listing, you need to authenticate (just fake this by entering a userId), so we know which user is creating the listing.


## Rules

- It should not be possible to create a listing with duplicate barcodes in it or within another listing.
- It should be possible for the last buyer of a ticket, to create a listing with that ticket (based on barcode).
- Use whatever framework you want.

## Instalation

A PHP7, Composer and MySQL server must be present to run the project.

On the project root folder run the following commands:
- `composer install`
- `php /bin/console doctrine:database:create`
- `php /bin/console doctrine:migrations:migrate`

If every dependency is met, all that is needed to do in order to execute this game is to open a terminal and run from the root dir:
`php /bin/console server:run`

This command will give you an url, just click it. and you should be good to go.

## How to use

This provides 4 users that can be accessed by their id's (1 to 4) on the log in page

Since there are no Listings yet you will need to "log in" to create the first listing.

After logging in on the top bar you have the 'Create Listing' link, give it a Description(A name of a Band, an event, something abstract, be creative) and a price.

Then provide some ticket Barcodes and you are good to go!

Log out, try to buy something as a guest.

Log in as another user, Buy something and sell it again, have some profit :D

Try to input some ticket barcode that you inserted with the other user.
